const express = require('express');
const _ = require('lodash');
const bodyParser = require('body-parser');
const {mongoose} = require('./db/mongoose');
const {User} = require('./models/user');
const {Rating} = require('./models/rating');
const {Candidate} = require('./models/candidate');
const {Item} = require('./models/item');
const {Test} = require('./models/test');
const admin = require("firebase-admin");
const converter = require('google-currency');

const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);

var serviceAccount = require("./path/to/bitbuy-bitbuy-firebase-adminsdk-l10wr-61a35f42b4.json");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://bitbuy-bitbuy.firebaseio.com"
});

app.use(bodyParser.json());

const port = process.env.PORT || 3000;

app.use(express.static(__dirname + '/public'));

app.use(function(req, res, next) {

    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Expose-Headers", "x-auth"); // allow the client side get the header
    res.header("Access-Control-Allow-Headers", "x-auth,  Content-Type, Accept"); //  allows getting both header and data from client

    next();
});

var authenticate = (req, res, next) => {

    var token = req.header('x-auth');

    admin.auth().verifyIdToken(token)
        .then(function(decodedToken) {
            // console.log(decodedToken.uid);

            var uid = decodedToken.uid;

            // res.send(uid);
            var user = {
                _id : uid,
                name: decodedToken.name,
                providerID: decodedToken.firebase.sign_in_provider
            };
            req.user = user;
            req.token = token;

            next();
        }).catch(function(error) {

        if(error.code === 'auth/argument-error' && error.message.includes('expired')){
            return res.status(402).send('refreshToken');
        } else {
            return res.status(401).send(error);
        }
    });
}

// user registration
// When a user first signing-in with Google/Facebook account, the userID of that user registered to auth table in Firebase,
// Next times when the user signing-in again with Google/Facebook account, Firebase auth checks if the user with the particular email exists with the providerID (Google/Facebook),
// if no such email and provider existed - Firebase auth creates a new record with that email and providerID. If that email and providerID already exist - Firebase auth sign the user in.
// In case of email and password registration, if the email doesn't exist - Firebase auth creates a new record with that email and make a new UID for that user,
// If that email is already exists the registration fails.
app.post('/userReg', (req, res) => {

    var idToken = req.header('x-auth');

    if(idToken){
        admin.auth().verifyIdToken(idToken)
            .then(function(decodedToken) {

                var uid = decodedToken.uid;

                var userData = {
                    _id : uid,
                    name: decodedToken.name,
                    providerID: decodedToken.firebase.sign_in_provider
                };

                var user = new User(userData);
                user.save().then( (user) => {   // sign-in by Google/Facebook/Email, if the user doesn't exist - save the user to the user collection
                    res.status(200).send(user);
                }).catch( (err) => {

                    if(err.code == 11000){      // sign-in by Google/Facebook, if the user exists in the user collection already - make a response with code 200 to sign the user in
                        return res.status(200).send({status: 'ok'});
                    }
                    res.status(400).send(err);
                });

            }).catch(function(error) {
            res.status(400).send(error);
        });
    }

});

app.get('/getItems', (req, res) => {

    // Item.find({boardVisibility: 'MainBoard'}).then( (docs) => {
    //     res.send(docs);
    // }).catch( (err) => {
    //     res.status(401).send(err);
    // });

    var page = req.query.page;

    Item.paginate({boardVisibility: 'MainBoard'}, { page, limit: 24 }, function(err, result) {
        res.send(result);
    });

});

// TODO add 'authenticate' middleware to this route
app.post('/postItem', authenticate, (req, res) => {

    console.log(req.body);

    new Item({
        userID: req.user._id,
        name: req.body.name,
        price: req.body.price,
        description:  req.body.description,
        picture: req.body.picture,
        date: new Date()
    }).save().then( item => {
        res.send(item);
    }).catch( err => {
        res.status(401).send(err);
    })
});

// TODO add 'authenticate' middleware to this route
// app.post('/getUserItems', (req, res) => {
//
//     var userID = req.user._id;
//
//     Item.find({_id: userID}).lean().then(items => {
//         getConvertedValue(item.price).then( value => {
//             item.convertedValue = value.converted;
//             res.send(item);
//         });
//     }).catch( err => {
//         res.status(401).send();
//     });
// })

// TODO add 'authenticate' middleware to this route
app.get('/getUserItems', authenticate, (req, res) => {

    var userID = req.user._id;

    Item.find({userID: userID}).then(items => {
        res.send(items);
    }).catch( err => {
        res.status(401).send();
    });

})

// var getConvertedValue = function (amount) {
//
//     const options = {
//         from: "ILS",
//         to: "XBT",
//         amount: amount
//     }
//
//     return converter(options).then(value => {
//         return value;
//     }).catch( e => e)
// }

// TODO add 'authenticate' middleware to this route
app.post('/deleteItem', authenticate, (req, res) => {

    var itemID = req.body.itemID;

    Item.findOneAndRemove({_id: itemID}).then(item => {
        if(item){
            res.send(item);
        } else {
            throw 'item number not found';
        }
    }).catch( err => {
        res.status(401).send(err);
    });
})

// TODO add 'authenticate' middleware to this route
// check validation, add to schema required
app.post('/updateItem', authenticate, (req, res) => {

    var item = req.body;

    Item.update({_id: req.body.id}, item , { runValidators: true }).then( item => {
        res.send(item)
    }).catch( err => {
        res.status(401).send(err);
    });

})

// app.post('/getUserInfo', authenticate, (req, res) => {
//
//    var uID = req.body.userID;
//
//     User.findOne({ _id: uID}).then( (user) => {
//
//     });
// });

// TODO add 'authenticate' middleware to this route
app.post('/getUserData', authenticate, (req, res) => {

    var uID = req.user._id;

    if(uID){
        User.findOne({ _id: uID}).then( (user) => {
            res.send(user)
        }).catch( (err) => {
           res.status(401).send(err);
        });
    }
});

// TODO add 'authenticate' middleware to this route
app.post('/getUserRatingAvg', authenticate, (req, res) => {

    var uid = req.body.userID;

    if(uid){

        User.findOne({_id: uid}).then( (user) => {

            Rating.aggregate([
                {$match: {"userID": uid}},
                {
                    $group: {
                        _id: "$userID",
                        ratingAvg: {$avg: '$rating'}
                    }
                }
            ]).then((result) => {

                var userProfile = {
                    name: user.name,
                    averageRating: result[0].ratingAvg,
                }

                res.send(userProfile);

            }).catch((err) => {
                res.status(401).send(err);
            });
        });
    }

});

// get the all reviews of a particular user, including the name of the reviewer, the review itself and rating
app.post('/getUserReviews', authenticate, (req, res) => {

    var uid = req.body.id;

    Rating.find({userID: uid}).then( (ratings) => {

        Promise.all(ratings.map(function (rating) {

            return User.findOne({_id: rating.reviewerID}).then( (user) => {

                return {
                     name: user.name,
                     text: rating.text,
                     rating: rating.rating
                 }
            });

        })).then( (arr) => {

            // console.log(arr);
            res.send(arr);

        }).catch( (err) => {
            res.status(401).send(err);
        });
    });

});


// TODO add 'authenticate' middleware to this route
app.post('/userUpdate', authenticate, (req, res) => {

    var uID = req.user._id;

    var user = req.body;

    User.update({ _id: uID}, user, {runValidators: true}).then( (user) => {
        res.send(user)
    }).catch( (err) => {
        res.status(401).send(err);
    });

});

// TODO add 'authenticate' middleware to this route
app.post('/userDelete', authenticate, (req, res) => {

    var uid = req.user._id;

    User.findOneAndRemove({ _id: uid}).then( (user) => {

        Item.remove({userID: user._id}).then(item => {

            Rating.remove({userID: uid}).then(ratings => {
                res.send();
            }).catch(err => {
                res.status(401).send(err);
            })
        }).catch(err => {
            res.status(401).send(err);
        })
    }).catch( (err) => {
        res.status(401).send(err);
    });

});

// TODO add 'authenticate' middleware to this route
// app.post('/getUserTransactions', (req, res) => {
//
//     var uID = req.body.id;
//
//     if(uID){
//         Transaction.find({ _id: uID}).then( (user) => {
//             res.send(user)
//         }).catch( (err) => {
//             res.status(401).send(err);
//         });
//     }
// });

// app.get('/calAvg', (req, res) => {
//
//     var x = "res";
//
//     Test.aggregate(
//         { $match: {"name": "zohar"}},
//         {
//             $group: {
//                 // _id: "$name",
//                 _id: "$name",
//                 avgAmount: { $avg: '$quantity' }
//             }
//         }
//     ).then( (result) => {
//         console.log(result[0].avgAmount);
//         res.send(result);
//     }).catch( (err) => {
//         res.status(401).send(err);
//     });
//
// })

app.post('/test2', (req, res) => {

    console.log(req.body);

    new Test({
        subject: req.body.subject,
        content: req.body.content,
        likes: req.body.likes,
        year: req.body.year,
        language: req.body.language
    }).save().then(result => {
       res.send(result);
    });

});

app.get('/testSearch', (req, res) => {

    var page = req.query.page;
    var search = req.query.search;

    // Item.paginate({$text: {$search: search}, boardVisibility: 'MainBoard'}, { page, limit: 10 }, function(err, result) {
    //     res.send(result);
    // });

    Item.find({$text: {$search: search}}).then(items => {
        res.send(items);
    }).catch(err => {
        res.status(401).send(err);
    })

});

app.get('/getTestResult', (req, res) => {

    // var search = req.body.search;
    //
    // Item.find({$text: {$search: search}}).then(result => {
    //     res.send(result);
    // }).catch(err => {
    //     res.status(401).send(err);
    // })

    var page = req.query.page;

    Test.paginate({}, { page, limit: 10 }, function(err, result) {
        res.send(result);
    });
});

// Game.textSearch('3d', function (err, output) {
//     if (err) return handleError(err);
//
//     var inspect = require('util').inspect;
//     console.log(inspect(output, { depth: null }));
//
//     // { queryDebugString: '3d||||||',
//     //   language: 'english',
//     //   results:
//     //    [ { score: 1,
//     //        obj:
//     //         { name: 'Super Mario 64',
//     //           _id: 5150993001900a0000000001,
//     //           __v: 0,
//     //           tags: [ 'nintendo', 'mario', '3d' ] } } ],
//     //   stats:
//     //    { nscanned: 1,
//     //      nscannedObjects: 0,
//     //      n: 1,
//     //      nfound: 1,
//     //      timeMicros: 77 },
//     //   ok: 1 }
// });
// });

// TODO add 'authenticate' middleware to this route
// The owner of the REAL money, who wants to buy the item
app.post('/applyToBuy', authenticate, (req, res) => {

    Candidate.findOne({candidateID: req.user._id, itemID: req.body.itemID}).then(result => {
        if(! result){
            Candidate({
                userID: req.body.userID,
                // candidateID: req.body.candidateID,
                candidateID: req.user._id,
                itemID: req.body.itemID
            }).save().then( (result) => {
                res.send(result);
            }).catch(e => {
                res.status(401).send(e);
            });
        } else{
            throw {code: '11200', text: 'duplicate record'}
        }
    }).catch( err => {

        res.status(401).send(err);
    })

});

// TODO add 'authenticate' middleware to this route
// As the owner of the bits, get the candidates who wants to buy the item
app.post('/getCandidates', (req, res) => {

    Candidate.find({itemID: req.body.itemID}).then( (result) => {

        Promise.all(result.map(candidate => {

            return User.findOne({_id: candidate.candidateID}).then(user => {

                return {
                    candidateID: user._id,
                    candidateName: user.name,
                    status: candidate.status
                }
            })

        })).then( (candidateArr) => {
            res.send(candidateArr);

        }).catch(err => {
            res.status(401).send(err);
        });

    }).catch(e => {
        res.status(401).send(e);
    });
});

// first get all the documents that the candidateID listed in, then get the item name of each itemID and then get the username of each userID (Promise.all with 2 promises for finding item name then username)

// // TODO add 'authenticate' middleware to this route
// As the owner of the REAL money, check the status of the request to buy the item
app.post('/getCandidateStatus', authenticate, (req, res) => {

        Candidate.find({candidateID: req.user._id}).then( (candidacies) => {

        // res.send(candidacies);
        // getItemAndUser(candidacies).then(obj => {
        //
        //     var newArr = obj.items.map( (item, i) => {
        //         return {
        //             item,
        //             username: obj.users[i].name
        //         }
        //     })
        //
        //     res.send(newArr);
        // }).catch(err => {
        //     console.log(err);
        // });

        // if candidateID not found, throw an error
        // if(_.isEmpty(candidacies)){
        //     throw 'not found candidate';
        // }

        Promise.all(candidacies.map(candidate => {
            return Item.findOne({_id: candidate.itemID}).then(item => {
                return User.findOne({_id: item.userID}).then(user => {

                    return Rating.aggregate([
                        {$match: {"userID": user._id}},
                        {
                            $group: {
                                _id: "$userID",
                                ratingAvg: {$avg: '$rating'}
                            }
                        }
                    ]).then((result) => {

                        return {
                            _id: item._id,
                            name: item.name,
                            price: item.price,
                            boardVisibility: item.boardVisibility,
                            candidateStatus: candidate.status,
                            date: item.date,
                            username: user.name,
                            userAvg: result[0].ratingAvg
                        }

                    })
                })
            })
        })).then(result => {
            res.send(result);
        }).catch(err => {
            res.status(401).send(err);
        })

    }).catch( err => {
        console.log(err);
        res.status(401).send(err);
    });
});

var getItemAndUser = async function (candidacies) {
   var items = await getItem(candidacies);
   var users = await getUser(candidacies);

   return {items, users};
}

var getItem = function (candidacies) {
    return new Promise( (resolve, reject) => {
         Promise.all(candidacies.map(candidate => {
           return Item.findOne({_id: candidate.itemID}).then(item => {

               return item
           });
        })).then( arr => {
            resolve(arr);
        }).catch(err => {
            return err;
         })
    });
}

var getUser = function (candidacies) {
    return new Promise( (resolve, reject) => {
        Promise.all(candidacies.map(candidate => {
            return User.findOne({_id: candidate.userID}).then(user => user);
        })).then( arr => {
            resolve(arr);
        }).catch(err => {
            return err;
        });
    });
}

// TODO add 'authenticate' middleware to this route
// As the owner of the REAL money, remove its candidacy to buy the item
app.post('/deleteCandidacy', authenticate, (req, res) => {
    Candidate.findOneAndRemove({itemID: req.body.itemID, candidateID: req.user._id}).then( (item) => {
        res.send(item);
    }).catch( err => {
        res.status(401).send(err);
    });
});

// TODO add 'authenticate' middleware to this route
app.post('/selectCandidate', authenticate, (req, res) => {

    var chosenCandidate = req.body.candidateID;

    Candidate.update({ itemID: req.body.itemID, candidateID: chosenCandidate}, {status: 'ACCEPTED'}).then( (result) => {

        Candidate.find({itemID: req.body.itemID}).then(candidates => {

            Promise.all(candidates.map(candidate => {
                if(candidate.candidateID != chosenCandidate){
                    return Candidate.update({itemID: req.body.itemID, candidateID: candidate.candidateID}, {status: 'REJECTED'})
                }
            })).then( (res2) => {

                Item.update({_id: req.body.itemID}, {boardVisibility: 'PublisherBuyerBoard'} , { runValidators: true }).then( updateResult => {
                    res.send(updateResult);
                }).catch( err => {
                    res.status(401).send(err);
                });

            }).catch(err => {
                res.status(401).send(err);
            })

        });

    }).catch(e => {
        res.status(401).send(e);
    });
});

app.post('/postRating', authenticate, (req, res) => {

    var rating = new Rating({
        userID: req.user._id,
        reviewerID: req.body.reviewerID,
        text: req.body.text,
        rating: req.body.rating
    });

    rating.save().then( (rate) => {
        res.send(rate);
    }).catch( (e) => {
        res.status(401).send(e);
    });
});

app.post('/testToken', authenticate, (req, res) => {

    idToken = req.header('x-auth');

    // * Get Google uid with tokenID *

    // idToken = req.query.idtoken;
    admin.auth().verifyIdToken(idToken)
        .then(function(decodedToken) {
            var uid = decodedToken.uid;
            // console.log(uid);
            res.send(uid);

        }).catch(function(error) {
        console.log(error);
        if(error.code === 'auth/argument-error' && error.message.includes('expired')){
            return res.status(402).send('refreshToken');
        }
        res.status(401).send(error);
    });

});

app.get('/convert', (req, res) => {

    const options = {
        from: "ILS",
        to: "XBT",
        amount: req.query.amount
    }

    converter(options).then(value => {
        res.send(value);
    }).catch( e => res.status(401).send(e));
});

/* Socket.io */

// app.get('/chat', function(req, res){
//
//     res.sendFile(__dirname + '/public/index.html');
//
//     if(! arr.find(element => element.itemID === req.query.itemID)){
//         itemID = req.query.itemID;
//         arr.push({itemID});
//         console.log(arr);
//     }
//
// });

// app.get('/arr', function(req, res){
//     arr.push(req.query.name);
//     console.log(arr);
//     res.send();
// });

io.on('connection', function(socket){

    socket.on('chat message', function(msg){
        // socket.to(<socketid>).emit('hey', 'I just met you');
        // console.log(msg);
        socket.to(msg.id).emit('chat message', {id: socket.id, message: msg.message});
        // io.emit('chat message', msg);
    });

});

const arr = [];

app.get('/chat', function(req, res){

    res.sendFile(__dirname + '/public/index.html');

    // console.log(req.query.itemID);

    io.on('connection', function(socket){

        var i = arr.find(element => element.itemID === req.query.itemID);

        if(! arr.find(element => element.itemID === req.query.itemID)){
            arr.push({itemID: req.query.itemID, user: socket.id});
            // console.log('arr: ', arr);
        } else {
            // socket.to(msg.id).emit('chat message', {id: socket.id, message: msg.message});
            socket.to(i.user).emit('welcome', {id: socket.id});
        }

        console.log('a user  connected ', socket.id);
        socket.on('disconnect', function(){
            console.log('user disconnected');
        });

        socket.on('welcome', function (data) {
            socket.to(data.id).emit('welcome', {id: socket.id});
        })

    });
});

// io.on('connection', function(socket){
//
//     // if(! arr.find(element => element.itemID === req.query.itemID)){
//     //     var obj = arr[arr.length - 1];
//     //     obj.userID = socket.id;
//     //     console.log('arr: ', arr);
//     // } else {
//     //     socket.emit('')
//     //     socket.to(msg.id).emit('chat message', {id: socket.id, message: msg.message});
//     // }
//
//     console.log('a user connected ', socket.id);
//     socket.on('disconnect', function(){
//         console.log('user disconnected');
//     });
//
// });


http.listen(port, function(){
    console.log('Start on port ', port);
});

