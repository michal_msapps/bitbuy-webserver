
var mongoose = require('mongoose');

mongoose.Promise = global.Promise;

// local database
// mongoose.connect(process.env.MONGODB_URI || 'mongodb://localhost:27017/Test123', (err) => {
//     console.log("Local DB");
// });

// mLab database
mongoose.connect('mongodb://bitbuymsapps:bb123321@ds119060.mlab.com:19060/bitbuy', (err) => {
    console.log("mLab DB");
});

module.exports = {mongoose};

