const mongoose = require('mongoose');

var RatingSchema = new mongoose.Schema({
    userID: {           // userID is the user who gets the review
        type: String,
        required: true
    },
    reviewerID: {       // the user who submits the review
        type: String,
        required: true
    },
    text: {
        type: String
    },
    rating: {
        type: Number,
        required: true
    }
});

var Rating = mongoose.model('Rating', RatingSchema);

module.exports = {Rating}