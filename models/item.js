const mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

var ItemsSchema = new mongoose.Schema({
    userID: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    picture: {
        url: {
            type: String
        },
        name:{
            type: String
        }
    },
    date: {
        type: Date,
        default: new Date()
    },
    boardVisibility: {
        type: String,
        enum : ['MainBoard','PublisherBoard','PublisherBuyerBoard'],
        default: 'MainBoard'
    }
});

ItemsSchema.plugin(mongoosePaginate);

ItemsSchema.index({"name":"text"});


// ItemsSchema.methods.toJSON = function () {
//     var item = this;
//     var userObject = user.toObject();
//
//     return _.pick(item, ['_id', 'userID', 'category', 'date'])
// };

var Item = mongoose.model('Item', ItemsSchema);

module.exports = {Item}