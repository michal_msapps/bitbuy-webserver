const mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

// create our schema
var Test = mongoose.Schema({
    subject: {
        type: String
    },
    content: {
        type: String
    },
    likes: {
        type: Number
    },
    year: {
        type: Number
    },
    language: {
        type: String
    }
});

Test.plugin(mongoosePaginate);

// give our schema text search capabilities
// Test.plugin(textSearch);

// add a text index to the tags array
// Test.index({ tags: 'text' });

Test.index({"subject":"text"});


var Test = mongoose.model('Test', Test);

module.exports = {Test}