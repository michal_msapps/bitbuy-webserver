const mongoose = require('mongoose');

var UserSchema = new mongoose.Schema({

    _id: {
        type: String,
        minlength: 1,
        required: true,
        trim: true
    },
    providerID: {
        type: String,
    },
    name: {
        type: String,
        trim: true
    }
});

// UserSchema.methods.toJSON = function () {
//     var user = this;
//     var userObject = user.toObject();
//
//     return _.pick(userObject, ['_id', 'providerID', 'firstName', 'lastName'])
// };

var User = mongoose.model('User', UserSchema);

module.exports = {User}

