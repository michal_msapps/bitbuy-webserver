const mongoose = require('mongoose');

var CandidateSchema = new mongoose.Schema({
    // the userID who has the bits
    userID: {
        type: String,
        required: true
    },
    // the userID who wants to buy the item (in real money)
    candidateID: {
        type: String
    },
    itemID: {
        type: String
    },
    status: {
        type: String,
        enum : ['PENDING','ACCEPTED','REJECTED'],
        default: 'PENDING'
    }

});

var Candidate = mongoose.model('Candidate', CandidateSchema);

module.exports = {Candidate}